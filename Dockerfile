# Pull base image 
FROM tomcat:8-jre8 
SHELL ["/bin/bash", "-c"]

# Maintainer 
MAINTAINER "fongang@outlook.fr"

RUN echo "Installing Git, Maven and Java"

RUN apt-get update && apt-get upgrade -y
RUN apt-get install git maven openjdk-8-jre -y

RUN echo "Building war file and copying it under ../tomcat/webapps directory"

RUN cd /tmp && git clone https://redtechvision@bitbucket.org/redtechvision/gcp1.git
RUN cd /tmp/gcp1 && mvn clean install
RUN cp /tmp/gcp1/target/webapp.war /usr/local/tomcat/webapps/webapp.war
RUN chmod 777 /usr/local/tomcat/webapps/webapp.war
VOLUME /usr/local/tomcat/webapps
EXPOSE 8080

#ADD /tmp/gcp1/target/webapp.war /usr/local/tomcat/webapps

#ADD ./webapp/target/webapp.war /usr/local/tomcat/webapps
